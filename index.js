
let http = require("http");

let port = 3000

let server = http.createServer((request, response) => {
	if (request.url == "/login") {
    response.writeHead(200, { "content-type": "text/plain" });
    response.end("This is the login page");
  } 
  else {
    response.writeHead(404, { "content-type": "text/plain" });
    response.end("Page Not Available");
  }
});


server.listen(port);

console.log(`Server is successfully running:${port}`)

